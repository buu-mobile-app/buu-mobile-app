import 'dart:async';
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/question_model.dart';

class GameState extends ChangeNotifier {
  List<Question> _questionList = getQuestions();
  int _currentQuestionIndex = 0;
  int _score = 0;
  Answer? _selectedAnswer;
  Timer? _countdownTimer;

  List<Question> get questionList => _questionList;
  int get currentQuestionIndex => _currentQuestionIndex;
  int get score => _score;
  Answer? get selectedAnswer => _selectedAnswer;
  Timer? get countdownTimer => _countdownTimer;

  set selectedAnswer(Answer? value) {
    _selectedAnswer = value;
    notifyListeners();
  }

  void startTimer() {
    _countdownTimer =
        Timer.periodic(Duration(seconds: 1), (_) => setCountDown(context));
  }

  Duration _myDuration = Duration(seconds: 10);
  Duration get myDuration => _myDuration;

  void stopTimer() {
    _countdownTimer!.cancel();
    notifyListeners();
  }

  void resetTimer() {
    stopTimer();
    _myDuration = Duration(seconds: 10);
    notifyListeners();
  }

  void setCountDown(context) {
    final reduceSecondsBy = 1;
    final seconds = _myDuration.inSeconds - reduceSecondsBy;
    if (seconds < 0) {
      stopTimer();
      // _next();
      bool isLastQuestion = false;
      if (currentQuestionIndex == questionList.length - 1) {
        isLastQuestion = true;
      }
      resetTimer();
      if (isLastQuestion) {
        showDialog(context: context, builder: (_) => _showScoreDialog(context));
      } else {
        _selectedAnswer = null;
        _currentQuestionIndex++;
        startTimer();
      }
    } else {
      _myDuration = Duration(seconds: seconds);
      notifyListeners();
    }
  }

  void nextQuestion(context) {
    bool isLastQuestion = false;
    if (_currentQuestionIndex == _questionList.length - 1) {
      isLastQuestion = true;
    }
    resetTimer();
    if (isLastQuestion) {
      showDialog(
        context: context,
        builder: (_) => _showScoreDialog(context),
      );
    } else {
      _currentQuestionIndex++;
      _selectedAnswer = null;
      startTimer();
      notifyListeners();
    }
  }

  Widget _showScoreDialog(context) {
    bool isPassed = false;
    if (_score >= _questionList.length * 0.6) {
      isPassed = true;
    }
    String title = isPassed ? "Passed" : "Failed";
    return AlertDialog(
      title: Text(title),
      content: Text("You scored $_score out of ${_questionList.length}"),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("OK"),
        )
      ],
    );
  }
}
