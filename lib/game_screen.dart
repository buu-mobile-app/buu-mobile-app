import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/question_model.dart';
import 'package:provider/provider.dart';

class GameScreen extends StatefulWidget {
  @override
  State<GameScreen> createState() => _GameScreenState();
}

class CounterModel extends ChangeNotifier {
  int currentQuestionIndex = 0;

  void increment() {
    currentQuestionIndex += 1;
    print('dd $currentQuestionIndex');
    notifyListeners();
  }
}

class TextTitle extends StatelessWidget {
  final int length;
  final CounterModel counterModel;
  const TextTitle(
      {super.key, required this.length, required this.counterModel});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => counterModel,
      builder: (context, child) => Text(
        "โจทย์คำถาม ${counterModel.currentQuestionIndex + 1}/$length",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}

class _GameScreenState extends State<GameScreen> {
  List<Question> questionList = getQuestions();
  int score = 0;
  Answer? selecteAnswer;
  Timer? countdownTimer;

  CounterModel counterModel = CounterModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimer();
  }

  void startTimer() {
    countdownTimer =
        Timer.periodic(Duration(seconds: 1), (_) => setCountDown());
  }

  Duration myDuration = Duration(seconds: 10);
  void stopTimer() {
    setState(() => countdownTimer!.cancel());
  }

  void resetTimer() {
    stopTimer();
    setState(() => myDuration = Duration(seconds: 10));
  }

  void setCountDown() {
    final reduceSecondsBy = 1;
    setState(() {
      final seconds = myDuration.inSeconds - reduceSecondsBy;
      if (seconds < 0) {
        countdownTimer!.cancel();
        _next();
      } else {
        myDuration = Duration(seconds: seconds);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final seconds = strDigits(myDuration.inSeconds.remainder(60));
    return Scaffold(
      backgroundColor: Colors.green,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
        child: SingleChildScrollView(
          child: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const Text(
                    "เกมคณิตหรรษา",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 40,
                    ),
                  ),
                  Icon(
                    Icons.access_alarm,
                    size: 40,
                    color: Colors.amber,
                  ),
                  Text(
                    "$seconds",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                  _questionWidget(),
                  _answerList(),
                  _nextButton(),
                ]),
          ),
        ),
      ),
    );
  }

  _questionWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.max,
      children: [
        TextTitle(
          length: questionList.length,
          counterModel: counterModel,
        ),
        const SizedBox(height: 20),
        Container(
          alignment: Alignment.center,
          width: double.infinity,
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
              color: Colors.orangeAccent,
              borderRadius: BorderRadius.circular(16)),
          child: Text(
            questionList[CounterModel().currentQuestionIndex].question,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 27,
              fontWeight: FontWeight.w600,
            ),
          ),
        )
      ],
    );
  }

  _answerList() {
    return Column(
      children: questionList[CounterModel().currentQuestionIndex]
          .answerList
          .map(
            (e) => _answerButton(e),
          )
          .toList(),
    );
  }

  Widget _answerButton(Answer answer) {
    bool isSelected = answer == selecteAnswer;

    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 8),
      height: 48,
      child: ElevatedButton(
        child: Text(answer.answer),
        style: ElevatedButton.styleFrom(
          shape: const StadiumBorder(),
          primary: isSelected ? Colors.deepPurple : Colors.white,
          onPrimary: isSelected ? Colors.white : Colors.black,
        ),
        onPressed: () {
          if (selecteAnswer == null) {
            if (answer.isCorrect) {
              score++;
            }
            setState(() {
              selecteAnswer = answer;
            });
          }
        },
      ),
    );
  }

  _nextButton() {
    bool isLastQuestion = false;
    if (counterModel.currentQuestionIndex == questionList.length - 1) {
      isLastQuestion = true;
    }
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      height: 48,
      child: ElevatedButton(
        child: Text(isLastQuestion ? "Submit" : "Next"),
        style: ElevatedButton.styleFrom(
          shape: const StadiumBorder(),
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          _next();
        },
      ),
    );
  }

  void _next() {
    counterModel.increment();
    print(counterModel.currentQuestionIndex);
    bool isLastQuestion = false;
    if (counterModel.currentQuestionIndex == questionList.length - 1) {
      isLastQuestion = true;
    }
    resetTimer();
    if (isLastQuestion) {
      showDialog(context: context, builder: (_) => _showScoreDialog());
    } else {
      setState(() {
        selecteAnswer = null;

        startTimer();
      });
    }
  }

  _showScoreDialog() {
    bool isPassed = false;
    if (score >= questionList.length * 0.6) {
      isPassed = true;
    }
    String title = isPassed ? "Passed" : "Failed";
    return AlertDialog(
      title: Text(
        title + " | Score is $score",
        style: TextStyle(color: isPassed ? Colors.green : Colors.red),
      ),
      content: ElevatedButton(
        child: const Text("Restart"),
        onPressed: () {
          Navigator.pop(context);
          setState(() {
            CounterModel().currentQuestionIndex = 0;
            score = 0;
            selecteAnswer = null;
          });

          resetTimer();
          startTimer();
        },
      ),
    );
  }
}
