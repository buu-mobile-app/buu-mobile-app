class Question{
final String question;
final List<Answer> answerList;

Question(this.question , this.answerList);
}

class Answer{
  final String answer;
  final bool isCorrect;

  Answer(this.answer, this.isCorrect);

}

List<Question> getQuestions() {
  List<Question> list = [];
  //ADD question and answer here

  list.add(Question(
    "2 + 10 = ?",
    [
      Answer("14", false),
      Answer("10", false),
      Answer("12", true),
      Answer("7", false),
    ]
  ));
  list.add(Question(
    "4 + 5 = ?",
    [
      Answer("13", false),
      Answer("9", true),
      Answer("23", false),
      Answer("5", false),
    ]
  ));
  list.add(Question(
    "15 - 4 = ?",
    [
      Answer("11", true),
      Answer("6", false),
      Answer("9", false),
      Answer("10", false),
    ]
  ));
  list.add(Question(
    "20 - 4 = ?",
    [
      Answer("12", false),
      Answer("10", false),
      Answer("16", true),
      Answer("0", false),
    ]
  ));
  list.add(Question(
    "2 x 4 = ?",
    [
      Answer("12", false),
      Answer("6", false),
      Answer("32", false),
      Answer("8", true),
    ]
  ));
  list.add(Question(
      "2 x 2 = ?",
      [
        Answer("12", false),
        Answer("6", false),
        Answer("7", false),
        Answer("4", true),
      ]
  ));
  return list;
}