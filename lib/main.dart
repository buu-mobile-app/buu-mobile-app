import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:provider/provider.dart';
import 'game_screen.dart';
import 'mainPage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'MATH GAME ',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),

        home: ChangeNotifierProvider(
          create: (context) => CounterModel(),
          child: Scaffold(
            backgroundColor: Colors.green[300],
            body: const SafeArea(
              child: MainPage(),
            ),
          ),
        ),
        // home: GameScreen()
      ),
    );
  }
}
