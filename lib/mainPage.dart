import 'package:flutter/material.dart';
import 'game_screen.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            //const SizedBox(height: 50.0),
            const Spacer(flex: 5),
            //const FlutterLogo(size: 100.0),
            Image.asset(
              "assets/images/Mathgame logo.png",
            ),
            //const Expanded(flex: 8,child: const FlutterLogo(size: 100.0)),
            //const SizedBox(height: 100.0),

            //const SizedBox(height: 30.0),
            const Spacer(flex: 3),

            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  fixedSize: Size(200, 100),
                  backgroundColor: Colors.teal,
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return GameScreen();
                  }));
                },
                child: const Text(
                  'เริ่มเกม',
                  style: TextStyle(fontSize: 35, color: Colors.yellow),
                )),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
